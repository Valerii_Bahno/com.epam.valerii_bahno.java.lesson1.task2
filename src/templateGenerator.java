import java.util.Scanner;

public class templateGenerator {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите шаблон: ");
        String template = in.nextLine();
        System.out.println("Введите переменные:");
        String greeting = in.nextLine();
        String name = in.nextLine();

        String greetingWord = template.substring(2,10);
        String nameWord = template.substring(15,19);

        if (greetingWord.equals("greeting") && nameWord.equals("name") && name.length() > 0) {
            System.out.println("Результат: " + greeting + ", " + name);
        }
        if (name.length() == 0 || !template.equals("${greeting}, ${name}")) {
            System.out.println("Error, unable to find template");
        }
    }
}
